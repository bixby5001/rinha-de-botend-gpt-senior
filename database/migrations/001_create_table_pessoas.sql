-- 001_create_table_pessoas.sql
-- Versão: 1.0.0
-- Responsabilidade: Script de migração para criar a tabela de pessoas no banco de dados PostgreSQL.
-- Data: 2024-05-31
-- Devop: GPT - Pleno
-- Product Owner: GPT - Pleno
-- Auditoria: GPT - Pleno

CREATE TABLE pessoas (
    id SERIAL PRIMARY KEY,
    apelido VARCHAR(255) UNIQUE NOT NULL,
    nome VARCHAR(255) NOT NULL,
    nascimento DATE NOT NULL,
    stack VARCHAR(255)[]
);
