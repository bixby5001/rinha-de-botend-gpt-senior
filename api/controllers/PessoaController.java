// PessoaController.java
// Versão: 1.0.0
// Responsabilidade: Controlador para gerenciar requisições relacionadas a Pessoa.
// Data: 2024-05-31
// Devop: GPT-Senior
// Product Owner: Trainspotting

package com.example.backend.controllers;

import com.example.backend.events.PessoaCreationEvent;
import com.example.backend.models.Pessoa;
import com.lmax.disruptor.dsl.Disruptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pessoa")
public class PessoaController {

    @Autowired
    private Disruptor<PessoaCreationEvent> disruptor;

    @PostMapping
    public String createPessoa(@RequestBody Pessoa pessoa) {
        PessoaCreationEvent event = new PessoaCreationEvent();
        event.setApelido(pessoa.getApelido());
        event.setNome(pessoa.getNome());
        event.setNascimento(pessoa.getNascimento());
        event.setStack(pessoa.getStack());

        disruptor.publishEvent((eventInstance, sequence) -> {
            eventInstance.setApelido(pessoa.getApelido());
            eventInstance.setNome(pessoa.getNome());
            eventInstance.setNascimento(pessoa.getNascimento());
            eventInstance.setStack(pessoa.getStack());
        });

        return "Pessoa criada com sucesso!";
    }
}
