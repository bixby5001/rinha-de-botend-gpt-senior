// Pessoa.java
// Versão: 1.0.0
// Responsabilidade: Representa uma entidade Pessoa com atributos como apelido, nome, data de nascimento e stack de habilidades.
// Data: 2024-05-31
// Devop: GPT - Pleno
// Product Owner: GPT - Pleno
// Auditoria: GPT - Pleno

package com.example.backend.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
public class Pessoa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Apelido é obrigatório")
    @Size(max = 255, message = "Apelido não pode ter mais que 255 caracteres")
    private String apelido;

    @NotBlank(message = "Nome é obrigatório")
    @Size(max = 255, message = "Nome não pode ter mais que 255 caracteres")
    private String nome;

    @NotNull(message = "Data de nascimento é obrigatória")
    @Past(message = "Data de nascimento deve ser no passado")
    private LocalDate nascimento;

    @NotNull(message = "Stack não pode ser nula")
    private String[] stack;

    // Construtor vazio necessário para o JPA
    public Pessoa() {
    }

    // Construtor com todos os atributos
    public Pessoa(String apelido, String nome, LocalDate nascimento, String[] stack) {
        this.apelido = apelido;
        this.nome = nome;
        this.nascimento = nascimento;
        this.stack = stack;
    }

    // Getters e Setters omitidos para brevidade

    // Métodos equals e hashCode para garantir consistência em coleções
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pessoa pessoa = (Pessoa) o;
        return id != null && id.equals(pessoa.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    // Método toString para representação textual do objeto
    @Override
    public String toString() {
        return "Pessoa{" +
                "id=" + id +
                ", apelido='" + apelido + '\'' +
                ", nome='" + nome + '\'' +
                ", nascimento=" + nascimento +
                ", stack=" + Arrays.toString(stack) +
                '}';
    }
}
