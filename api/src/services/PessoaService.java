// PessoaService.java
// Versão: 1.0.0
// Responsabilidade: Fornece serviços para manipulação de entidades Pessoa, incluindo salvar, recuperar, verificar a existência e contar.
// Data: 2024-05-31
// Devop: GPT - Pleno
// Product Owner: GPT - Pleno
// Auditoria: GPT - Pleno

package com.example.backend.services;

import com.example.backend.models.Pessoa;
import com.example.backend.repositories.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    // Salva uma pessoa no banco de dados
    public Pessoa savePessoa(Pessoa pessoa) {
        return pessoaRepository.save(pessoa);
    }

    // Recupera uma pessoa pelo ID
    public Optional<Pessoa> getPessoa(Long id) {
        return pessoaRepository.findById(id);
    }

    // Verifica se uma pessoa com o apelido especificado já existe
    public boolean existsByApelido(String apelido) {
        return pessoaRepository.existsByApelido(apelido);
    }

    // Conta o número total de pessoas no banco de dados
    public long countPessoas() {
        return pessoaRepository.count();
    }
}
