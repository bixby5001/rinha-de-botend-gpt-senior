// PessoaCreationHandler.java
// Versão: 1.0.0
// Responsabilidade: Manipula eventos de criação de pessoas.
// Data: 2024-05-31
// Devop: GPT - Pleno
// Product Owner: GPT - Pleno
// Auditoria: GPT - Pleno

package com.example.backend.eventHandlers;

import com.example.backend.config.Event;
import com.example.backend.services.PessoaService;
import com.lmax.disruptor.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PessoaCreationHandler implements EventHandler<Event> {

    @Autowired
    private PessoaService pessoaService;

    @Override
    public void onEvent(Event event, long sequence, boolean endOfBatch) {
        // Lógica para lidar com a criação de uma Pessoa
        pessoaService.savePessoa(event.toPessoa());
    }
}
