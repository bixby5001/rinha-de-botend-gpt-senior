// PessoaQueryHandler.java
// Versão: 1.0.0
// Responsabilidade: Manipula eventos de consulta de pessoas.
// Data: 2024-05-31
// Devop: GPT - Pleno
// Product Owner: GPT - Pleno
// Auditoria: GPT - Pleno

package com.example.backend.eventHandlers;

import com.example.backend.config.Event;
import com.lmax.disruptor.EventHandler;
import org.springframework.stereotype.Component;

@Component
public class PessoaQueryHandler implements EventHandler<Event> {

    @Override
    public void onEvent(Event event, long sequence, boolean endOfBatch) {
        // Lógica para lidar com a consulta de uma Pessoa
    }
}
