// PessoaQueryHandler.java
// Versão: 1.0.0
// Responsabilidade: Manipula eventos de consulta de pessoas.
// Data: 2024-05-31
// Devop: GPT - Pleno
// Product Owner: GPT - Pleno
// Auditoria: GPT - Pleno

package com.example.backend.eventHandlers;

import com.example.backend.config.Event;
import com.example.backend.models.Pessoa;
import com.example.backend.services.PessoaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PessoaQueryHandler implements EventHandler<Event> {

    private static final Logger logger = LoggerFactory.getLogger(PessoaQueryHandler.class);

    @Autowired
    private PessoaService pessoaService;

    @Override
    public void onEvent(Event event, long sequence, boolean endOfBatch) {
        try {
            logger.info("Handling Pessoa query event for ID: {}", event.getId());
            
            // Consulta a pessoa pelo ID
            Pessoa pessoa = pessoaService.getPessoa(event.getId())
                    .orElseThrow(() -> new RuntimeException("Pessoa not found with ID: " + event.getId()));
            
            // Log a pessoa encontrada
            logger.info("Found Pessoa: {}", pessoa);
            
            // Adicione aqui a lógica adicional necessária para lidar com a consulta
            // Por exemplo, você pode enviar a pessoa encontrada para outro serviço ou realizar operações adicionais.
            
        } catch (Exception e) {
            // Log de exceção
            logger.error("Error handling Pessoa query event for ID: {}", event.getId(), e);
            // Você pode lidar com a exceção de forma apropriada, como lançar ou enviar para um sistema de monitoramento.
        }
    }
}
