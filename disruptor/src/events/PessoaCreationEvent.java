// PessoaCreationEvent.java
// Versão: 1.0.0
// Responsabilidade: Representa um evento de criação de Pessoa.
// Data: 2024-05-31
// Devop: GPT - Pleno
// Product Owner: GPT - Pleno
// Auditoria: GPT - Pleno

package com.example.backend.events;

import com.example.backend.models.Pessoa;

public class PessoaCreationEvent extends Event {

    // Converte o evento em uma entidade Pessoa
    public Pessoa toPessoa() {
        Pessoa pessoa = new Pessoa();
        pessoa.setApelido(this.getApelido());
        pessoa.setNome(this.getNome());
        pessoa.setNascimento(this.getNascimento());
        pessoa.setStack(this.getStack());
        return pessoa;
    }
}
