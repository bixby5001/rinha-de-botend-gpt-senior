# rinha-de-botend-gpt-senior

Este projeto contém um sistema de gerenciamento de "Pessoas" com várias tecnologias e arquiteturas de backend, incluindo um balanceador de carga com NGINX, um serviço de API com Spring Boot, um banco de dados PostgreSQL, eventos com LMAX Disruptor, persistência em memória com Prevayler e testes de carga com Gatling.


Estrutura do Projeto
api: Serviço principal da API construído com Spring Boot.
database: Configuração do banco de dados PostgreSQL.
nginx: Configuração do NGINX para balanceamento de carga.
disruptor: Implementação de eventos com LMAX Disruptor.
prevayler: Persistência em memória com Prevayler.
stress-test: Testes de carga com Gatling.
docker-compose.yml: Arquivo Docker Compose para orquestrar todos os serviços. 


Como Executar
Clone o repositório:


git clone https://github.com/bixby5001/rinha-de-botend-gpt-senior.git
cd rinha-de-botend-gpt-senior


Construa as imagens Docker:


docker-compose build
Inicie os serviços:


docker-compose up -d
Acesse a API:


http://localhost:8080/api/pessoas


Tecnologias Utilizadas
Spring Boot
PostgreSQL
NGINX
LMAX Disruptor
Prevayler
Gatling
Docker


Documentação
API
Banco de Dados
NGINX
Disruptor
Prevayler
Testes de Carga

### Passos Iniciais para Configurar e Executar o Projeto

Para começar, siga os passos abaixo para configurar, construir e testar o projeto:

1. **Instalar Dependências do Maven**:
   Navegue para o diretório `api`, `disruptor`, e `prevayler` e execute o comando Maven para instalar as dependências de cada um:

   ```bash
   cd api
   mvn clean install
   cd ../disruptor
   mvn clean install
   cd ../prevayler
   mvn clean install
   ```

2. **Construir Imagens Docker**:
   Após instalar todas as dependências, construa as imagens Docker para cada serviço. Certifique-se de estar no diretório raiz do projeto onde o `docker-compose.yml` está localizado.

   ```bash
   docker-compose build
   ```

3. **Executar o Docker Compose**:
   Levante todos os serviços definidos no `docker-compose.yml`.

   ```bash
   docker-compose up
   ```

4. **Testar a Aplicação**:
   Com todos os serviços em execução, você pode usar a ferramenta de teste de carga para verificar o comportamento da API. O serviço de teste de carga é definido no `docker-compose.yml` e pode ser executado com:

   ```bash
   docker-compose run stress-test
   ```

### Gerar Javadoc em Português

Para gerar a documentação Javadoc em português, siga os passos abaixo:

1. **Adicionar Configuração Javadoc no `pom.xml`**:
   Certifique-se de que o `pom.xml` de cada módulo (`api`, `disruptor`, `prevayler`) tenha a configuração correta para gerar Javadoc em português. Aqui está um exemplo de como pode ser a configuração:

   ```xml
   <build>
       <plugins>
           <plugin>
               <groupId>org.apache.maven.plugins</groupId>
               <artifactId>maven-javadoc-plugin</artifactId>
               <version>3.3.1</version>
               <configuration>
                   <locale>pt_BR</locale>
                   <doclet>standard</doclet>
                   <encoding>UTF-8</encoding>
                   <outputDirectory>${project.build.directory}/site/apidocs</outputDirectory>
               </configuration>
           </plugin>
       </plugins>
   </build>
   ```

2. **Gerar Javadoc**:
   Navegue para o diretório de cada módulo (`api`, `disruptor`, `prevayler`) e execute o comando Maven para gerar o Javadoc.

   ```bash
   mvn javadoc:javadoc
   ```

### Documentação do Projeto

A seguir, a documentação em formato Javadoc para as principais classes do projeto:

#### `PessoaController.java`

```java
/**
 * Controlador para gerenciar requisições relacionadas a Pessoa.
 * 
 * @versão 1.0.0
 * @data 2024-05-31
 * @devop GPT-Senior
 * @productOwner Trainspotting
 */
public class PessoaController {
    // Métodos de endpoint para operações CRUD relacionadas a Pessoa
}
```

#### `Pessoa.java`

```java
/**
 * Modelo de dados para a entidade Pessoa.
 * 
 * @versão 1.0.0
 * @data 2024-05-31
 * @devop GPT-Senior
 * @productOwner Trainspotting
 */
public class Pessoa {
    // Campos e métodos para a entidade Pessoa
}
```

#### `PessoaService.java`

```java
/**
 * Serviço para operações de negócios relacionadas a Pessoa.
 * 
 * @versão 1.0.0
 * @data 2024-05-31
 * @devop GPT-Senior
 * @productOwner Trainspotting
 */
public class PessoaService {
    // Métodos de negócios para operações com Pessoa
}
```

#### `001_create_table_pessoas.sql`

```sql
-- Script de migração para criar a tabela de pessoas.
-- @versão 1.0.0
-- @data 2024-05-31
-- @devop GPT-Senior
-- @productOwner Trainspotting

CREATE TABLE pessoas (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(100) NOT NULL,
    idade INT NOT NULL,
    email VARCHAR(100) NOT NULL
);
```

### Estrutura do Projeto

A estrutura do projeto é organizada da seguinte maneira:

- **api**: Serviço principal da API construído com Spring Boot.
- **database**: Configuração do banco de dados PostgreSQL.
- **nginx**: Configuração do NGINX para balanceamento de carga.
- **disruptor**: Implementação de eventos com LMAX Disruptor.
- **prevayler**: Persistência em memória com Prevayler.
- **stress-test**: Testes de carga com Gatling.
- **docker-compose.yml**: Arquivo Docker Compose para orquestrar todos os serviços.

### Arquivos e Diretórios

        ```plaintext
        rinha-de-botend-gpt-senior/
        ├── api/
        │   ├── controllers/
        │   │   └── PessoaController.java
        │   ├── Dockerfile
        │   ├── pom.xml
        │   ├── README.md
        │   ├── src/
        │   │   ├── models/
        │   │   │   └── Pessoa.java
        │   │   └── services/
        │   │       └── PessoaService.java
        ├── database/
        │   ├── Dockerfile
        │   ├── migrations/
        │   │   └── 001_create_table_pessoas.sql
        │   └── README.md
        ├── disruptor/
        │   ├── Dockerfile
        │   ├── pom.xml
        │   ├── README.md
        │   ├── src/
        │   │   ├── eventHandlers/
        │   │   │   ├── PessoaCreationHandler.java
        │   │   │   └── PessoaQueryHandler.java
        │   │   └── events/
        │   │       ├── PessoaCreationEvent.java
        │   │       └── PessoaQueryEvent.java
        ├── docker-compose.yml
        ├── LICENSE
        ├── nginx/
        │   ├── Dockerfile
        │   ├── nginx.conf
        │   └── README.md
        ├── prevayler/
        │   ├── Dockerfile
        │   ├── pom.xml
        │   ├── README.md
        │   └── src/
        │       └── data/
        │           └── PessoaData.java
        ├── README.md
        └── stress-test/
            ├── Dockerfile
            ├── run-test.sh
            ├── README.md
            ├── resources/
            │   ├── pessoas-payloads.tsv
            │   └── termos-busca.tsv
            └── simulations/
                └── RinhaBackendSimulation.scala
        ```

        Com essas informações, você está pronto para configurar, construir, testar e documentar o projeto `rinha-de-botend-gpt-senior`.


Contribuições
São bem-vindas contribuições para este projeto. Por favor, siga as diretrizes de contribuição do GitHub.


Licença
Este projeto está licenciado sob a licença MIT.


Devop: GPT-Senior
Auditoria: GPT- Pleno
Product Owner: @trainsppotting
Data
2024-05-31

Versão
1.0.5

Status:

o arrombado @OpenAI junta velharia de boas. clap, clap, clap

        (x) spring requisições http
        (x) prevayler in memory cap/caos/entropia 
        (x) disruptor a 10MM TPS
        (x) nginx balanceador de carga
        (x) sql retrô
        mas
        (x) não sabe (ainda) versionar no maven