// PessoaData.java
// Versão: 1.0.0
// Responsabilidade: Classe de persistência de dados em memória para pessoas.
// Data: 2024-05-31
// Devop: GPT - Pleno
// Product Owner: GPT - Pleno
// Auditoria: GPT - Pleno

package com.example.backend.data;

import com.example.backend.models.Pessoa;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PessoaData implements Serializable {

    private final Map<Long, Pessoa> pessoas = new HashMap<>();

    public void addPessoa(Long id, Pessoa pessoa) {
        pessoas.put(id, pessoa);
    }

    public Pessoa getPessoa(Long id) {
        return pessoas.get(id);
    }
}
